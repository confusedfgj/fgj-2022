using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MusicScript : MonoBehaviour
{
    
    public AudioSource audio;
    public AudioClip puzzle;
    public AudioClip nature;
    public AudioClip industry;
    public AudioClip natEndSound;
    public AudioClip indEndSound;
    public AudioClip between;
    public static bool natWin = false;
    public static bool indWin = false;
    public static bool puzzleOn = false;
    public static bool natEnd = false;
    public static bool indEnd = false;
    public static bool bothEnd = false;

    // Start is called before the first frame update
    void Start()
    {
        audio = GetComponent<AudioSource>();
        audio.Play();
        DontDestroyOnLoad(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        if(natWin && !puzzleOn) {
            audio.clip = nature;
        }
        else if(indWin && !puzzleOn) {
            audio.clip = industry;
        }
        else if(puzzleOn) {
            audio.clip = puzzle;
        }
        else if (natEnd) {
            audio.clip = natEndSound;
        }
        else if (indEnd) {
            audio.clip = indEndSound;
        }
        else if (bothEnd) {
            audio.clip = between;
        }
        if (!audio.isPlaying) audio.Play();
    }
}
