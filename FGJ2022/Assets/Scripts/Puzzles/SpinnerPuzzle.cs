using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Random = UnityEngine.Random;

public class SpinnerPuzzle : BasePuzzle
{
    [SerializeField] private Sprite[] techSprites = new Sprite[4];
    [SerializeField] private Sprite[] natureSprites = new Sprite[4];
    [SerializeField] private Sprite[] allSprites = new Sprite[0];
    [SerializeField] private List<SpinnerPuzzleSpinner> _spinners;
    [SerializeField] private GameObject techBtn, natureBtn, neutralBtn;
    private bool clickable = true;

    private void Start()
    {
        // int winningTechSprite = Random.Range(0, 4);
        // int winningNatureSprite = Random.Range(0, 4);
        // foreach (SpinnerPuzzleSpinner spinner in _spinners)
        // {
        //     spinner.icons.ForEach(icon =>
        //     {
        //         icon.color = new Color(0, 0, 0);
        //         icon.sprite = Random.Range(0f, 1f) > 0.5f
        //             ? techSprites[Random.Range(0, techSprites.Length)]
        //             : natureSprites[Random.Range(0, natureSprites.Length)];
        //     });
        //
        //     spinner.icons[Random.Range(0, 4)].sprite = techSprites[winningTechSprite];
        //     spinner.icons[Random.Range(4, 8)].sprite = natureSprites[winningNatureSprite];
        // }
        _spinners.ForEach(spinner =>
        {
            var slots = new List<int>() {0, 1, 2, 3, 4, 5, 6, 7};
            for (int i = 0; i < slots.Count; i++) 
            {
                int temp = slots[i];
                int randomIndex = Random.Range(i, slots.Count);
                slots[i] = slots[randomIndex];
                slots[randomIndex] = temp;
            }

            for (int i = 0; i < spinner.icons.Count; i++)
            {
                spinner.icons[i].color = Color.black;
                spinner.icons[i].sprite = allSprites[slots[i]];
            }
        });
    }

    private void RowOne(SpriteRenderer icon, Vector2 dir)
    {
        var hits = Physics2D.RaycastAll(icon.transform.position, dir, 3f, LayerMask.GetMask("Symbol"));
        hits.OrderBy(hit => Vector2.Distance(icon.transform.position, hit.transform.position));
        Debug.DrawRay(icon.transform.position, dir.normalized * 3f, Color.red, 0.1f);
        for (int i = 0; i < hits.Length; i++)
        {
            SpriteRenderer sprite = hits[i].collider.GetComponent<SpriteRenderer>();
            if (sprite.sprite == icon.sprite)
            {
                icon.color = Color.white;
                sprite.color = Color.white;
            }
            else
            {
                sprite.color = Color.black;
                if (i == 0) icon.color = Color.black;
                for (int j = i; j < hits.Length; j++)
                {
                    hits[j].collider.GetComponent<SpriteRenderer>().color = Color.black;
                }
                // techBtn.SetActive(false);
                // natureBtn.SetActive(false);
                // neutralBtn.SetActive(true);
                return;
            }
        }
        techBtn.SetActive(techSprites.Contains(icon.sprite));
        natureBtn.SetActive(!techSprites.Contains(icon.sprite));
        neutralBtn.SetActive(false);
        // onCompletePuzzle.Invoke(techSprites.Contains(icon.sprite));
        // Destroy(gameObject);
    }

    private void Update()
    {
        if (!_spinners.Any(spinner => spinner.spinning)) _spinners[0].icons.ForEach((icon) => RowOne(icon, -icon.transform.up));

        if (!Input.GetMouseButtonDown(0)) return;
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero, 1000, LayerMask.GetMask("Puzzle"));
        if (hit.collider != null)
        {
            if (hit.collider.CompareTag("TechWin") || hit.collider.CompareTag("NatureWin"))
            {
                onCompletePuzzle.Invoke(hit.collider.CompareTag("TechWin"));
                Destroy(gameObject);
                return;
            }
            SpinnerPuzzleSpinner spinr = hit.collider.GetComponent<SpinnerPuzzleSpinner>();
            spinr.target =
                Quaternion.Euler(new Vector3(0, 0, spinr.target.eulerAngles.z + (hit.point.x > 0 ? 45 : -45)));
            foreach (SpinnerPuzzleSpinner spinner in spinr.counterSpinners)
            {
                spinner.target =
                    Quaternion.Euler(new Vector3(0, 0, spinner.target.eulerAngles.z + (hit.point.x > 0 ? -45 : 45)));
                if (!spinner.spinning) StartCoroutine(spinner.SpinnySpinner());
                spinner.spinning = true;
            }
            foreach (SpinnerPuzzleSpinner spinner in spinr.sameDirSpinners)
            {
                spinner.target =
                    Quaternion.Euler(new Vector3(0, 0, spinner.target.eulerAngles.z + (hit.point.x > 0 ? 45 : -45)));
                if (!spinner.spinning) StartCoroutine(spinner.SpinnySpinner());
                spinner.spinning = true;
            }

            if (!spinr.spinning) StartCoroutine(spinr.SpinnySpinner());
            spinr.spinning = true;
        }
    }
}
