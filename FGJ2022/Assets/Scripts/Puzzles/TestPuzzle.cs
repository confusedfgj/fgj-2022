using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;
using UnityEngine.UI;

public class TestPuzzle : BasePuzzle
{
    [SerializeField] private Button winButton;
    [SerializeField] private Button loseButton;

    void Start()
    {
        winButton.onClick.AddListener(() =>
        {
            onCompletePuzzle.Invoke(true);
            Destroy(gameObject);
        });
        
        loseButton.onClick.AddListener(() =>
        {
            onCompletePuzzle.Invoke(false);
            Destroy(gameObject);
        });
    }
}
