using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;
using UnityEngine.Serialization;

public class BasePuzzle : MonoBehaviour
{

    public UnityEvent onStartPuzzle = new UnityEvent();
    public UnityEvent<bool> onCompletePuzzle = new UnityEvent<bool>();

    private void OnDisable()
    {
        onStartPuzzle.RemoveAllListeners();
        onCompletePuzzle.RemoveAllListeners();
    }

    private void OnDestroy()
    {
        onStartPuzzle.RemoveAllListeners();
        onCompletePuzzle.RemoveAllListeners();
    }
}
