using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PicturePuzzle : BasePuzzle
{
    [SerializeField]
    private Transform photoPlace1;
    [SerializeField]
    private Transform photoPlace2;
    [SerializeField]
    private Transform photoPlace3;
    [SerializeField]
    private Transform photoPlace4;
    [SerializeField]
    private Transform photo1;
    [SerializeField]
    private Transform photo2;
    [SerializeField]
    private Transform photo3;
    [SerializeField]
    private Transform photo4;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(photoPlace1.transform.position == photo1.transform.position && 
        photoPlace2.transform.position == photo2.transform.position && 
        photoPlace3.transform.position == photo3.transform.position && 
        photoPlace4.transform.position == photo4.transform.position)
        {
            Debug.Log("Nature wins");
            onCompletePuzzle.Invoke(false);
            Destroy(gameObject);
        }
        else if(photoPlace1.transform.position == photo4.transform.position && 
        photoPlace2.transform.position == photo3.transform.position && 
        photoPlace3.transform.position == photo2.transform.position && 
        photoPlace4.transform.position == photo1.transform.position)
        {
            Debug.Log("Tech wins");
            onCompletePuzzle.Invoke(true);
            Destroy(gameObject);
        }
    }
}
