using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PicturePlacement : MonoBehaviour
{
    [SerializeField]
    private Transform photoPlace1;
    [SerializeField]
    private Transform photoPlace2;
    [SerializeField]
    private Transform photoPlace3;
    [SerializeField]
    private Transform photoPlace4;
    private Vector2 startPosition;
    private Vector2 mousePosition;
    private float deltaX, deltaY;
    [SerializeField] private float snapDistance = 1;

    // Start is called before the first frame update
    void Start()
    {
        startPosition = transform.position;
    }

    private void OnMouseDown() 
    {
        deltaX = Camera.main.ScreenToWorldPoint(Input.mousePosition).x - transform.position.x;
        deltaY = Camera.main.ScreenToWorldPoint(Input.mousePosition).y - transform.position.y;
    }

    private void OnMouseDrag() 
    {
        mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector2(mousePosition.x - deltaX, mousePosition.y - deltaY);
    }

    private void OnMouseUp()
    {
        if(Mathf.Abs(transform.position.x - photoPlace1.position.x) <= snapDistance &&
           Mathf.Abs(transform.position.y - photoPlace1.position.y) <= snapDistance)
            {
                transform.position = new Vector2(photoPlace1.position.x, photoPlace1.position.y);
            }
        else if(Mathf.Abs(transform.position.x - photoPlace2.position.x) <= snapDistance &&
                Mathf.Abs(transform.position.y - photoPlace2.position.y) <= snapDistance)
            {
                transform.position = new Vector2(photoPlace2.position.x, photoPlace2.position.y);
            }
        else if(Mathf.Abs(transform.position.x - photoPlace3.position.x) <= snapDistance &&
                Mathf.Abs(transform.position.y - photoPlace3.position.y) <= snapDistance)
            {
                transform.position = new Vector2(photoPlace3.position.x, photoPlace3.position.y);
            }
        else if(Mathf.Abs(transform.position.x - photoPlace4.position.x) <= snapDistance &&
                Mathf.Abs(transform.position.y - photoPlace4.position.y) <= snapDistance)
            {
                transform.position = new Vector2(photoPlace4.position.x, photoPlace4.position.y);
            }
        else
        {
             transform.position = new Vector2(startPosition.x, startPosition.y);
        }
    }
}
