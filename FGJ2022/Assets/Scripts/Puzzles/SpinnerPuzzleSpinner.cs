using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

public class SpinnerPuzzleSpinner : MonoBehaviour
{
    public List<SpriteRenderer> icons;
    public List<SpinnerPuzzleSpinner> counterSpinners;
    public List<SpinnerPuzzleSpinner> sameDirSpinners;
    private const float Spinspeed = 10f;
    public bool spinning = false;
    public Quaternion target;

    private void Start()
    {
        target = transform.rotation;
    }

    public IEnumerator SpinnySpinner()
    {
        while (Quaternion.Angle(transform.rotation, target) > 0)
        {
            transform.rotation = Quaternion.Slerp( transform.rotation, target, Spinspeed * Time.deltaTime);
            yield return null;
        }

        transform.rotation = target;
        spinning = false;
    }
}
