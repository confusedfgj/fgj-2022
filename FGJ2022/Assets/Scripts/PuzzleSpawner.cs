using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PuzzleSpawner : MonoBehaviour
{
    [SerializeField] private GameObject puzzleToSpawn;

    [SerializeField] private Transform puzzleParent;

    [SerializeField] private GameObject tech, nature, technature, puzzleBG;

    private GameObject _activePuzzleGO;
    private BasePuzzle _activePuzzle;
    // Start is called before the first frame update
    public void SpawnPuzzle()
    {
        if (GameController.puzzleIsOpen) return;

        MusicScript.puzzleOn = true;
        puzzleBG.SetActive(true);
        GameController.puzzleIsOpen = true;
        _activePuzzleGO = Instantiate(puzzleToSpawn, puzzleToSpawn.transform.position, Quaternion.identity, puzzleParent);
        _activePuzzle = _activePuzzleGO.GetComponent<BasePuzzle>();
        _activePuzzle.onCompletePuzzle.AddListener((succeeded) =>
        {
            if (succeeded)
            {
                GameController.techPoints++;
            }
            else
            {
                GameController.naturePoints++;
            }
            MusicScript.puzzleOn = false;
            technature.SetActive(false);
            puzzleBG.SetActive(false);
            tech.SetActive(succeeded);
            nature.SetActive(!succeeded);
            GameController.puzzleIsOpen = false;
            Destroy(gameObject);
        });
    }
}
