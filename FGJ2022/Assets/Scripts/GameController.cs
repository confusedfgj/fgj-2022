using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public static UnityEvent GameEndingEvent = new UnityEvent();
    public static bool puzzleIsOpen = false;

    public static int naturePoints, techPoints;
    
    private void Start()
    {
        GameEndingEvent.AddListener(() =>
        {
            if (naturePoints > techPoints)
            {
                SceneManager.LoadScene("NatureWin");
            }
            else if (techPoints > naturePoints)
            {
                SceneManager.LoadScene("TechWin");
            }
            else
            {
                SceneManager.LoadScene("DualityWin");
            }
        });
    }

    private void Update()
    {
        if (naturePoints + techPoints == 4)
        {
            MusicScript.natWin = naturePoints > techPoints;
            MusicScript.indWin = techPoints > naturePoints;
            MusicScript.natEnd = naturePoints > techPoints;
            MusicScript.indEnd = techPoints > naturePoints;
            MusicScript.bothEnd = techPoints == naturePoints;
            GameEndingEvent.Invoke();
            return;
        }

        MusicScript.natWin = naturePoints > techPoints;
        MusicScript.indWin = techPoints > naturePoints;
    }
}
