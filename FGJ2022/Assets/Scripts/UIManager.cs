using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public Button startButton;
    public Button creditsButton;
    public Button quitButton;
    public Button mainMenuButton;
    public Button backButton;
    public Button backGpButton;
    public Button soundButton;

    public GameObject startPanel;
    public GameObject gameplayPanel;
    public GameObject puzzlePanel;
    public GameObject creditsPanel;
    public GameObject soundOn;
    public GameObject soundOff;
    public AudioSource audioSource;
    public bool soundsOn = true;

    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 0;

        startButton.onClick.AddListener(StartGame);
        creditsButton.onClick.AddListener(Credits);
        quitButton.onClick.AddListener(Application.Quit);
        mainMenuButton.onClick.AddListener(MainMenu);
        backButton.onClick.AddListener(Back);
        backGpButton.onClick.AddListener(Gameplay);
        //soundButton.onClick.AddListener(Sounds);
    }

    void StartGame () {
        Time.timeScale = 1;
        SceneManager.LoadScene("GameScene");
        return;
        
        startPanel.SetActive(false);
        gameplayPanel.SetActive(true);
        creditsPanel.SetActive(false);
        puzzlePanel.SetActive(false);
    }

    void Credits () {
        startPanel.SetActive(false);
        gameplayPanel.SetActive(false);
        creditsPanel.SetActive(true);
        puzzlePanel.SetActive(false);
    }

    void Gameplay () {
        startPanel.SetActive(false);
        gameplayPanel.SetActive(true);
        creditsPanel.SetActive(false);
        puzzlePanel.SetActive(false);
    }

    void MainMenu () {
        Time.timeScale = 0;

        startPanel.SetActive(true);
        gameplayPanel.SetActive(false);
        creditsPanel.SetActive(false);
        puzzlePanel.SetActive(false);
    }

    void Back () {
        startPanel.SetActive(true);
        gameplayPanel.SetActive(false);
        creditsPanel.SetActive(false);
        puzzlePanel.SetActive(false);
    }

    void Sounds() {
        if(soundsOn) {
            audioSource.Stop();
            soundOn.SetActive(false);
            soundOff.SetActive(true);
            soundsOn = false;
        }
        else if(!soundsOn) {
            audioSource.Play();
            soundOn.SetActive(true);
            soundOff.SetActive(false);
            soundsOn = true;
        }
    }
}
